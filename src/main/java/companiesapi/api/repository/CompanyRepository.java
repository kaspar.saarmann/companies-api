package companiesapi.api.repository;

import companiesapi.api.model.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Repository
public class CompanyRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    //andmebaasist andmete küsimine
    public List<Company> getCompanies() { // tagastame listina
        return jdbcTemplate.query("select * from company", (row, number) -> {
            return new Company(
                    row.getInt("id"),
                    row.getString("name"),
                    row.getString("logo"),
                    row.getString("established"),
                    row.getInt("employees")
            );
        });
    }

    public Company getCompany(int id) {
        List<Company> companiesList = jdbcTemplate.query("select * from company where id = ?",
                new Object[]{id},
                (row, number) -> {
                    return new Company(
                            row.getInt("id"),
                            row.getString("name"),
                            row.getString("logo"),
                            row.getString("established"),
                            row.getInt("employees")

                    );
                });
        if (companiesList.size() > 0) {
            return companiesList.get(0);
        } else {
            return null;
        }
    }



    public void addCompany(Company company) {
        jdbcTemplate.update("insert into company (`name`, logo, established, employees) values(?, ?, ?, ?)",
                company.getName(), company.getLogo(), company.getEstablished(), company.getEmployees()
        );
    }

    //update meetod on nii insert, delete kui kõik muu muutmine
    public void deleteCompany(int id) {
        jdbcTemplate.update("delete from company where id = ?", id); // küsimärgi asemele paneme lihtsalt id
    }

    public void updateCompany(Company company) {
        jdbcTemplate.update("update company set `name` = ?, logo = ?, established = ?, employees = ? where id = ?",
                company.getName(), company.getLogo(), company.getEstablished(),
                company.getEmployees(), company.getId());

    }


}
