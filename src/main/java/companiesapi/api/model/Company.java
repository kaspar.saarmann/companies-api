package companiesapi.api.model;

public class Company {

    private int id;
    private String name;
    private String logo;
    private String established;
    private int employees;

    public Company(int id, String name, String logo, String established, int employees) {
        this.id = id;
        this.name = name;
        this.logo = logo;
        this.established = established;
        this.employees = employees;
    }

    public Company() {

    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLogo() {
        return logo;
    }

    public String getEstablished() {
        return established;
    }

    public int getEmployees() {
        return employees;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public void setEstablished(String established) {
        this.established = established;
    }

    public void setEmployees(int employees) {
        this.employees = employees;
    }
}
