package companiesapi.api.rest;

import companiesapi.api.model.Company;
import companiesapi.api.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/companies")
@CrossOrigin("*")   // lubame igaltpoolt ligipääsu, vajalik et front end pääseks ligi

public class CompanyController {

    @Autowired
    private CompanyRepository companyRepository;

    @GetMapping("/hello/{name}/{country}") // ilma selleta ei ole veebis nähtav
    public String getHelloWorld(@PathVariable("name") String name, @PathVariable("country") String country) {

        return "Tere " + name + "!, "+ "" + country;
    }

    @GetMapping
    public List<Company> getAllCompanies(){
        return companyRepository.getCompanies();
    }

    @GetMapping("/{id}")
    public Company getCompany(@PathVariable("id") int id) {
        return companyRepository.getCompany(id);
    }

    @PostMapping
    public void addCompany(@RequestBody Company company){
        companyRepository.addCompany(company);
    }

    @PutMapping
    public void updateCompany(@RequestBody Company company){
        companyRepository.updateCompany(company);
    }

    @DeleteMapping ("/{id}")
    public void deleteCompany(@PathVariable("id") int id){
        companyRepository.deleteCompany(id);
    }


}
